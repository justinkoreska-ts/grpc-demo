# gRPC Demo

## Quick Start

```
> go build ./cmd/server
> go build ./cmd/client
> export GRPC_HOST=127.0.0.1:7777
> ./server &
> ./client
```
