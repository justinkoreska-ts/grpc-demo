package main

import (
	"log"
	"net"
	"os"

	"bitbucket.org/justinkoreska-ts/grpc-demo/atlas"
	"google.golang.org/grpc"
)

func main() {

	listener, err := net.Listen("tcp", os.Getenv("GRPC_HOST"))
	if err != nil {
		log.Fatalf("net.Listen: %v", err)
	}

	atlasServer := &atlasServer{}
	grpcServer := grpc.NewServer()

	atlas.RegisterAtlasServer(grpcServer, atlasServer)

	grpcServer.Serve(listener)
}

type atlasServer struct {
	atlas.UnimplementedAtlasServer
}

var locations = []*atlas.Location{
	{Code: "YTO", Name: "Toronto"},
	{Code: "YVR", Name: "Vancouver"},
}

func (s *atlasServer) GetLocations(query *atlas.LocationQuery, stream atlas.Atlas_GetLocationsServer) error {

	for _, location := range locations {
		stream.Send(location)
	}

	return nil
}
