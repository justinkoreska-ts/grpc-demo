package main

import (
	"context"
	"log"
	"os"

	"bitbucket.org/justinkoreska-ts/grpc-demo/atlas"
	"google.golang.org/grpc"
)

func main() {

	conn, err := grpc.Dial(os.Getenv("GRPC_HOST"), grpc.WithInsecure())
	if err != nil {
		log.Fatalf("grpc.Dial: %v", err)
	}
	defer conn.Close()

	atlasClient := atlas.NewAtlasClient(conn)

	locationsClient, err := atlasClient.GetLocations(context.Background(), &atlas.LocationQuery{})
	if err != nil {
		log.Fatalf("atlasClient.GetLocations: %v", err)
	}

	var location atlas.Location
	for nil == locationsClient.RecvMsg(&location) {
		log.Printf("{ code = %v, name = %v }\n", location.Code, location.Name)
	}
}
