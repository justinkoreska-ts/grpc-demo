// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package atlas

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// AtlasClient is the client API for Atlas service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type AtlasClient interface {
	GetLocations(ctx context.Context, in *LocationQuery, opts ...grpc.CallOption) (Atlas_GetLocationsClient, error)
}

type atlasClient struct {
	cc grpc.ClientConnInterface
}

func NewAtlasClient(cc grpc.ClientConnInterface) AtlasClient {
	return &atlasClient{cc}
}

func (c *atlasClient) GetLocations(ctx context.Context, in *LocationQuery, opts ...grpc.CallOption) (Atlas_GetLocationsClient, error) {
	stream, err := c.cc.NewStream(ctx, &Atlas_ServiceDesc.Streams[0], "/atlas.Atlas/GetLocations", opts...)
	if err != nil {
		return nil, err
	}
	x := &atlasGetLocationsClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type Atlas_GetLocationsClient interface {
	Recv() (*Location, error)
	grpc.ClientStream
}

type atlasGetLocationsClient struct {
	grpc.ClientStream
}

func (x *atlasGetLocationsClient) Recv() (*Location, error) {
	m := new(Location)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// AtlasServer is the server API for Atlas service.
// All implementations must embed UnimplementedAtlasServer
// for forward compatibility
type AtlasServer interface {
	GetLocations(*LocationQuery, Atlas_GetLocationsServer) error
	mustEmbedUnimplementedAtlasServer()
}

// UnimplementedAtlasServer must be embedded to have forward compatible implementations.
type UnimplementedAtlasServer struct {
}

func (UnimplementedAtlasServer) GetLocations(*LocationQuery, Atlas_GetLocationsServer) error {
	return status.Errorf(codes.Unimplemented, "method GetLocations not implemented")
}
func (UnimplementedAtlasServer) mustEmbedUnimplementedAtlasServer() {}

// UnsafeAtlasServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to AtlasServer will
// result in compilation errors.
type UnsafeAtlasServer interface {
	mustEmbedUnimplementedAtlasServer()
}

func RegisterAtlasServer(s grpc.ServiceRegistrar, srv AtlasServer) {
	s.RegisterService(&Atlas_ServiceDesc, srv)
}

func _Atlas_GetLocations_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(LocationQuery)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(AtlasServer).GetLocations(m, &atlasGetLocationsServer{stream})
}

type Atlas_GetLocationsServer interface {
	Send(*Location) error
	grpc.ServerStream
}

type atlasGetLocationsServer struct {
	grpc.ServerStream
}

func (x *atlasGetLocationsServer) Send(m *Location) error {
	return x.ServerStream.SendMsg(m)
}

// Atlas_ServiceDesc is the grpc.ServiceDesc for Atlas service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Atlas_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "atlas.Atlas",
	HandlerType: (*AtlasServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "GetLocations",
			Handler:       _Atlas_GetLocations_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "atlas.proto",
}
